<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Countable;
use Iterator;

/**
 * HtmlCollectionNodeInterface interface file.
 * 
 * This interface specifies all the actions that every node that may have
 * children should be able to.
 * 
 * @author Anastaszor
 * @extends \Iterator<string, HtmlAbstractNodeInterface>
 */
interface HtmlCollectionNodeInterface extends Countable, HtmlAbstractNodeInterface, Iterator
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : HtmlAbstractNodeInterface;
	
	/**
	 * Searches the node tree for nodes with the given id, returns null if
	 * there is none.
	 *
	 * @param string $ident
	 * @return ?HtmlAbstractNodeInterface
	 */
	public function getElementById(string $ident) : ?HtmlAbstractNodeInterface;
	
	/**
	 * Searches the node tree for nodes with the given tag name. If there is
	 * none, an empty list is returned.
	 *
	 * @param string $name
	 * @return Iterator<integer, HtmlAbstractNodeInterface>
	 */
	public function getElementsByTagName(string $name) : Iterator;
	
	/**
	 * Removes all the nodes that have no attributes and no contents,
	 * recursively, on all nodes that are present in the subtree represented
	 * by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeEmpty() : HtmlCollectionNodeInterface;
	
	/**
	 * Removes all the nodes that are of type CData on all nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeCData() : HtmlCollectionNodeInterface;
	
	/**
	 * Removes all the comments on attributes and all comment nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeComments() : HtmlCollectionNodeInterface;
	
	/**
	 * Removes all the scripts on attributes and all script nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeScripts() : HtmlCollectionNodeInterface;
	
	/**
	 * Removes all the styles on attributes and all style nodes that are
	 * present in the subtree represented by this node.
	 *
	 * @return HtmlCollectionNodeInterface
	 */
	public function removeStyles() : HtmlCollectionNodeInterface;
	
	/**
	 * Transforms the html tree structure represented by this node to another
	 * structure according to the rules of the given transformer.
	 *
	 * @param HtmlTransformerInterface $transformer
	 * @return HtmlCollectionNodeInterface
	 */
	public function transformWith(HtmlTransformerInterface $transformer) : HtmlCollectionNodeInterface;
	
	/**
	 * Count the number of children nodes this collection contains. If type is
	 * null, all children are counted. If it is not, then only the nodes of
	 * given type are counted.
	 * 
	 * @param ?string $type
	 * @return integer
	 */
	public function countChildren(?string $type = null) : int;
	
	/**
	 * Gets whether this collection contains children nodes. If type is null,
	 * all children are considered. If it is not, then only the nodes of given
	 * type are considered.
	 * 
	 * @param ?string $type
	 * @return boolean
	 */
	public function hasChildren(?string $type = null) : bool;
	
	/**
	 * Gets the first node encountered on this list. If type is null, then
	 * all children are considered. If it is not, then only the nodes of given
	 * type are considered. If there are no such children or if the given
	 * position is more than the number of considered children, then null is
	 * returned. Positions are indexed at zero from the beginning of the list.
	 * Negative positions are interpreted as last child.
	 * 
	 * @param ?string $type
	 * @param integer $position
	 * @return ?HtmlAbstractNodeInterface
	 */
	public function getFirstChild(?string $type = null, int $position = 0) : ?HtmlAbstractNodeInterface;
	
	/**
	 * Gets the last node encountered on this list. If type is null, then
	 * all children are considered. If it is not, then only the nodes of given
	 * type are considered. If there are no such children or if the given
	 * position is more than the number of considered children, then null is
	 * returned. Positions are indexed at zero from the end of the list.
	 * Negative positions are interpreted as first child.
	 * 
	 * @param ?string $type
	 * @param integer $position
	 * @return ?HtmlAbstractNodeInterface
	 */
	public function getLastChild(?string $type = null, int $position = 0) : ?HtmlAbstractNodeInterface;
	
	/**
	 * Gets all the children of this list. If the type is null, then the given
	 * list is equivalent to this list. If it is not, then only the nodes of
	 * given type are considered. If there are no such children, then an empty
	 * list is returned. Positions in the new list are not guaranteed to be 
	 * indexed on the same indexes of this list.
	 * 
	 * @param ?string $type
	 * @return HtmlCollectionNodeInterface
	 */
	public function getChildren(?string $type = null) : HtmlCollectionNodeInterface;
	
}
