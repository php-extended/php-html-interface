<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Iterator;
use PhpExtended\Parser\ParseThrowable;
use Stringable;

/**
 * HtmlAbstractNodeInerface interface file.
 * 
 * This interface represents the root of a composite pattern.
 * 
 * @author Anastaszor
 * @abstract
 */
interface HtmlAbstractNodeInterface extends Stringable
{
	
	public const TYPE_DOCUMENT = 'document';
	public const TYPE_DOCTYPE = 'doctype';
	public const TYPE_COMMENT = 'comment';
	public const TYPE_CDATA = 'cdata';
	public const TYPE_TEXT = 'text';
	public const INDENT_TABS = "\t";
	public const INDENT_SPACES = '    ';
	
	/**
	 * Gets the name of this node. The name of the node is the string between
	 * the brackets like "html" in <html>...</html>.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the attributes of the node. The attributes of the node are the key
	 * values pairs that are inside the brackets of the node.
	 * 
	 * @return HtmlAttributeListInterface
	 */
	public function getAttributes() : HtmlAttributeListInterface;
	
	/**
	 * Gets whether this node is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this node has the attribute with the given name.
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function hasAttribute(string $name) : bool;
	
	/**
	 * Gets the attribute with the given name from this node, null if there is
	 * none.
	 * 
	 * @param string $name
	 * @return ?HtmlAttributeInterface
	 */
	public function getAttribute(string $name) : ?HtmlAttributeInterface;
	
	/**
	 * Gets the text value of this node and its descendants. Only meaningful 
	 * text is returned on this method, meaning text on comments, cdata, and
	 * similar nodes are not returned.
	 * 
	 * @return string
	 */
	public function getText() : string;
	
	/**
	 * Gets whether this node equals another node.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Dumps the given node and all its descendants into html format, with
	 * the given indentation string (default = tab) and the given indent depths.
	 * 
	 * @param string $indent
	 * @param integer $depths
	 * @return string the human-readable html
	 */
	public function dump(string $indent = self::INDENT_TABS, int $depths = 0) : string;
	
	/**
	 * Makes this node be visited by the given visitor.
	 * 
	 * @param HtmlVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(HtmlVisitorInterface $visitor);
	
	/**
	 * Counts the number of nodes in this whole html node tree.
	 * 
	 * @return integer
	 */
	public function getTreeNodeQuantity() : int;
	
	/**
	 * Counts the maximum depths of all nodes in this whole html node tree.
	 * 
	 * @return integer
	 */
	public function getTreeDepths() : int;
	
	/**
	 * Gets the length, in bytes, of meaningful contents in this html node
	 * tree.
	 * 
	 * @return integer
	 */
	public function getTreeContentLength() : int;
	
	/**
	 * Gets the length, in bytes, of full length if the whole tree should be
	 * rendered in compacted html.
	 * 
	 * @return integer
	 */
	public function getTreeHtmlLength() : int;
	
	/**
	 * Gets whether the given node matches the given selector.
	 * 
	 * @param string $cssSelector
	 * @return boolean true if it matches, false if it does not
	 * @throws ParseThrowable if the css selector is invalid
	 */
	public function matchesCss(string $cssSelector) : bool;
	
	/**
	 * Gets the node in the given tree that is at the nth position amongst nodes
	 * that matches. Positive positions are indexed by zero, negative positions
	 * start at the end (by -1 being the last element).
	 * 
	 * @param string $cssSelector
	 * @param integer $position
	 * @return ?HtmlAbstractNodeInterface
	 * @throws ParseThrowable if the css selector is invalid
	 */
	public function findNodeCss(string $cssSelector, int $position = 0) : ?HtmlAbstractNodeInterface;
	
	/**
	 * Gets all the nodes in the given tree that matches the given selector, 
	 * an empty collection is returned if none matches.
	 * 
	 * @param string $cssSelector
	 * @return Iterator<integer, HtmlAbstractNodeInterface>
	 * @throws ParseThrowable if the css selector is invalid
	 */
	public function findAllNodesCss(string $cssSelector) : Iterator;

	/**
	 * Filters this node by removing all nodes that matches the given css
	 * selector.
	 *
	 * @param string $cssSelector
	 * @return ?HtmlAbstractNodeInterface
	 * @throws ParseThrowable if the css selector is invalid
	 */
	public function filterCss(string $cssSelector) : ?HtmlAbstractNodeInterface;
	
}
