<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Stringable;

/**
 * HtmlTransformerInterface interface file.
 * 
 * This interface specifies how a transformer for any dom should behave.
 * 
 * @author Anastaszor
 */
interface HtmlTransformerInterface extends Stringable
{
	
	/**
	 * Do the transformation of any dom into another dom.
	 * 
	 * @param HtmlCollectionNodeInterface $dom
	 * @return HtmlCollectionNodeInterface
	 */
	public function transform(HtmlCollectionNodeInterface $dom) : HtmlCollectionNodeInterface;
	
}
