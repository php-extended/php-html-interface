<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Countable;
use Iterator;
use Stringable;

/**
 * HtmlAttributeListInterface interface file.
 * 
 * This interface represents an ordered list of attributes that can be
 * attached to a node.
 * 
 * @author Anastaszor
 * @extends \Iterator<string, HtmlAttributeInterface>
 */
interface HtmlAttributeListInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : HtmlAttributeInterface;
	
	/**
	 * Gets whether this list is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this attribute list has the attribute with the given name.
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function hasAttribute(string $name) : bool;
	
	/**
	 * Gets the attribute with the given name from this list, null if there is
	 * none.
	 * 
	 * @param string $name
	 * @return ?HtmlAttributeInterface
	 */
	public function getAttribute(string $name) : ?HtmlAttributeInterface;
	
	/**
	 * Gets whether this node list equals another node list.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
}
