<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Stringable;

/**
 * HtmlVisitorInterface interface file.
 * 
 * This interface specifies an object that can walk the Html Node Tree.
 * 
 * @author Anastaszor
 */
interface HtmlVisitorInterface extends Stringable
{
	
	/**
	 * Vbisits the given collection node.
	 * 
	 * @param HtmlCollectionNodeInterface $collection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitCollection(HtmlCollectionNodeInterface $collection);
	
	/**
	 * Visits the given single node.
	 * 
	 * @param HtmlSingleNodeInterface $single
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitSingle(HtmlSingleNodeInterface $single);
	
}
