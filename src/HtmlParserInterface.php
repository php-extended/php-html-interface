<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use PhpExtended\Parser\ParserInterface;

/**
 * HtmlParserInterface interface file.
 * 
 * This class represents a parser for html data.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<HtmlCollectionNodeInterface>
 */
interface HtmlParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
