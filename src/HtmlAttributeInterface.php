<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-html-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Html;

use Stringable;

/**
 * HtmlAttributeInterface interface file.
 * 
 * This interface represents a single attribute that can be attached to a node.
 * 
 * @author Anastaszor
 */
interface HtmlAttributeInterface extends Stringable
{
	
	/**
	 * Gets the html of the attribute.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the value of the attribute.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
	/**
	 * Gets whether the value of this attribute begins with the given value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueBeginsWith(string $value) : bool;
	
	/**
	 * Gets whether the value of this attribute contains the given value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueContains(string $value) : bool;
	
	/**
	 * Gets whether the value of this attribute ends with the given value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueEndsWith(string $value) : bool;
	
	/**
	 * Gets whether the value of this attribute is equal to the given value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueEquals(string $value) : bool;
	
	/**
	 * Gets whether one of the values of this attribute begins with the given
	 * value (followed by dash) or is the given value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueWordBeginsWith(string $value) : bool;
	
	/**
	 * Gets whether one of the values of this attribute is equal to the given
	 * value.
	 * 
	 * @param string $value
	 * @return boolean
	 */
	public function valueHasWord(string $value) : bool;
	
	/**
	 * Gets whether this attribute equals another attribute.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
}
